package com.ji.producer.controller;

import com.ji.api.OrderApi;
import com.ji.module.UserProto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * Title:
 * Description:
 * Author:吉
 * Since:2018/4/17
 * Version:1.1.0
 */
@RestController
public class OrderController implements OrderApi {

    @Value("${soft.user}")
    private String softUser;

    @Override
//    @RequestMapping("/getOrderAddrs")
    public String getOrderAddrs() {
        return "this is my order:" + softUser;
    }

    @Override
    //    @RequestMapping("/getOrderAddrsProto")
    public ResponseEntity getOrderAddrsProto() {
        return ResponseEntity.ok(UserProto.User.newBuilder().setId(1).setName("jichen").build());
    }

    @Override
    //    @RequestMapping("/getOrderAddrsProtoSingle")
    public UserProto.User getOrderAddrsProtoSingle() {
        return UserProto.User.newBuilder().setId(1).setName("jichen").build();
    }

    @Override
    //    @RequestMapping("/getDate")
    public Date getDate() {
        return new Date();
    }
}
