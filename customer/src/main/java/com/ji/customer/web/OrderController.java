package com.ji.customer.web;

/**
 * Title:
 * Description:
 * Author:吉
 * Since:2018/4/17
 * Version:1.1.0
 */

import com.ji.api.OrderApi;
import com.ji.customer.config.SSLSocketClient;
import com.ji.module.UserProto;
import feign.okhttp.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@RestController
public class OrderController {
    @Value("${aa.bb}")
    private String aabb;

    @Autowired
    private OrderApi orderService;
    @Autowired
    okhttp3.OkHttpClient okHttpClient;

    @RequestMapping("/getOrderAddrsByNactive")
    public String getOrderAddrsByNactive() throws IOException {
        Request request = new Request.Builder()
                .url("https://localhost:9000/getOrderAddrs")
                .build();
        Response response = okHttpClient.newCall(request).execute();
        return response.body().string();
    }

    @RequestMapping("/getOrderAddrs")
    public String getOrderAddrs() {
        return orderService.getOrderAddrs() + "|" + aabb;
    }


    @RequestMapping("/getOrderAddrsProto")
    public ResponseEntity getOrderAddrsProto() {
        return orderService.getOrderAddrsProto();
    }

    @RequestMapping("/getOrderAddrsProtoSingle")
    public UserProto.User getOrderAddrsProtoSingle() {
        return orderService.getOrderAddrsProtoSingle();
    }

    @RequestMapping("/getDate")
    public Date getDate() {
        return orderService.getDate();
    }
}
