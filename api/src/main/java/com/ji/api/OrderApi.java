package com.ji.api;

import com.ji.module.UserProto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

/**
 * Title:
 * Description:
 * Author:吉
 * Since:2018/4/17
 * Version:1.1.0
 */
@FeignClient(name = "https://producer")
public interface OrderApi {
    @RequestMapping(value = "/getOrderAddrs")
    String getOrderAddrs();

    @RequestMapping("/getOrderAddrsProto")
    ResponseEntity getOrderAddrsProto();

    @RequestMapping("/getOrderAddrsProtoSingle")
    UserProto.User getOrderAddrsProtoSingle();

    @RequestMapping("/getDate")
    Date getDate();
}
